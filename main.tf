module "nginx" {
  source              = "./modules"
  VPC_ID              = data.aws_vpc.vpc.id
  PRIVATE_SUBNETS     = data.terraform_remote_state.vpc.outputs.PRIVATE_SUBNETS
  SG                  = aws_security_group.allow_nginx.id
  NAME                = "frontend-prod"
}

resource "null_resource" "connection" {
  connection {
    host      = module.nginx.privateip
    user      = "root"
    password  = "DevOps321"
  }
  provisioner "remote-exec" {
    inline = [
    "yum install httpd -y","systemctl start httpd","systemctl enable httpd"]
  }
}