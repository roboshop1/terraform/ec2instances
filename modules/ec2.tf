resource "aws_instance" "instance" {
  ami           = "ami-0c84a3e93390c29bc"
  instance_type = "t2.micro"
  subnet_id     = element(tolist(var.PRIVATE_SUBNETS),0)
  tags = {
    Name = var.NAME
  }
  vpc_security_group_ids = [var.SG]
}

output "privateip" {
  value = aws_instance.instance.private_ip
}